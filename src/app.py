from sanic import Sanic, response
from sanic.log import logger
from sanic_openapi import doc, swagger_blueprint
from asyncpg import create_pool
import Model

import json

charfinder = Sanic(__name__)
charfinder.blueprint(swagger_blueprint)


@charfinder.listener('before_server_start')
async def register_db(app, loop):
    conn = "postgres://{user}@{host}:{port}/{database}".format(
        user='postgres', host='db', port=5432, database='postgres'
    )
    app.config['pool'] = await create_pool(
        dsn=conn,
        min_size=10,  # bytes
        max_size=10,  # bytes
        max_queries=50000,
        max_inactive_connection_lifetime=300,
        loop=loop
    )


@charfinder.listener('after_server_stop')
async def close_connection(app, loop):
    pool = app.config['pool']
    async with pool.acquire() as conn:
        await conn.close()


def jsonify(records):
    """
    Parse asyncpg record response into JSON format
    """
    list_return = []
    for r in records:
        itens = r.items()
        list_return.append({i[0]: i[1].rstrip() if type(
            i[1]) == str else i[1] for i in itens})
    return list_return

def unmangle(db_response):
    list_return = []
    for r in db_response:
        for i in r.items():
            list_return.append(i)
    return list_return[0][1]


@charfinder.route('/')
async def test(request):
    return response.json({'Hello': 'World'})


@charfinder.get('/characters/<character_name>/abilities')
@doc.response(200, {'character': str, 'abilities': [Model.Ability]}, description='Character abilities')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_abilities(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_abilities('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'abilities': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/domains')
@doc.response(200, {'character': str, 'domains': [Model.Domain]}, description='Character domains')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_domains(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_domains('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'domains': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/feats')
@doc.response(200, {'character': str, 'feats': [Model.Feat]}, description='Character feats')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_feats(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_feats('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'feats': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/gear')
@doc.response(200, {'character': str, 'gear': [Model.GearItem]}, description='Character gear')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_feats(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_gear('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'gear': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/info')
@doc.response(200, {'character': str, 'abilities': Model.Info}, description='Character info')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_feats(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_info('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'info': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/languages')
@doc.response(200, {'character': str, 'languages': [Model.Language]}, description='Character languages')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_languages(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_languages('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'languages': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/money')
@doc.response(200, {'character': str, 'money': [Model.Currency]}, description='Character money')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_money(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_money('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'money': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/skills')
@doc.response(200, {'character': str, 'skills': [Model.Skill]}, description='Character skills')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_skills(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_skills('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'skills': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/spells')
@doc.response(200, {'character': str, 'spells': [Model.Spell]}, description='Character spells')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_spells(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_spells('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'spells': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/stats')
@doc.response(200, {'character': str, 'stats': [Model.Stat]}, description='Character stats')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_stats(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_show_stats('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'stats': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/special-abilities')
@doc.response(200, {'character': str, 'special_abilities': [Model.SpecialAbility]},
              description='Character special abilities')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_stats(request, character_name):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT character_special_abilities('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            for el in resp:
                del el['character_id']  # Why was it there at all?
            return response.json({'character': character_name, 'special_abilities': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/armor_class')
@doc.response(200, {'character': str, 'armor_class': [Model.ArmorClass]}, description='Character armor class stats')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_armor_class(request, character_name: str):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT combat_stats_show_armor_class('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'armor_class': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/cmb')
@doc.response(200, {'character': str, 'cmb': [Model.Cmb]}, description='Character cmb stats')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_cmb(request, character_name: str):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT combat_stats_show_cmb('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'cmb': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/cmd')
@doc.response(200, {'character': str, 'cmd': [Model.Cmd]}, description='Character cmd stats')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_cmd(request, character_name: str):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT combat_stats_show_cmd('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'cmd': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/health')
@doc.response(200, {'character': str, 'health': [Model.Health]}, description='Character health info')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_health(request, character_name: str):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT combat_stats_show_health('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'health': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/saving-throws')
@doc.response(200, {'character': str, 'saving_throw': [Model.SavingThrow]},
              description='Character saving throw stats')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_saving_throw(request, character_name: str):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT combat_stats_show_saving_throws('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'saving_throw': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.get('/characters/<character_name>/speed')
@doc.response(200, {'character': str, 'speed': [Model.Speed]}, description='Character speed stats')
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def get_speed(request, character_name: str):
    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = f'''SELECT combat_stats_show_speed('{character_name}')'''
        rows = await conn.fetch(query)
        data = unmangle(rows)
        if data is not None:
            resp = json.loads(data)
            return response.json({'character': character_name, 'speed': resp})
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


@charfinder.put('/search')
@doc.response(200, [{'character': str, 'info': Model.Info}])
@doc.response(404, {'status': int, 'message': str}, description='Database error')
async def search_characters(request):
    def build_query():
        def order_by():
            def convert_name(name):
                if name == 'name':
                    return f"similarity(full_name, '{request.json[name]}')"
                else:
                    return f"similarity({name}, '{request.json[name]}')"
            if not len(request.json.keys()) == 0:
                return 'order by ' + ', '.join(map(convert_name, request.json.keys())) + ' desc'
            else:
                return 'order by full_name'
        query = 'select character_name, full_name, alignment, level, experience_points, next_level, deity, homeland, '\
              + 'race, size, gender, age, height, weight, hair, eyes '\
              + 'from character ch join info i on ch.character_id = i.character_id '\
              + order_by()
        return query

    pool = request.app.config['pool']
    async with pool.acquire() as conn:
        query = build_query()
        rows = await conn.fetch(query)
        logger.debug(rows)
        data = jsonify(rows)
        logger.debug(data)
        if data is not None:
            return response.json(data)
        else:
            return response.json({'status': 404, 'message': 'No entry in database'}, status=404)


if __name__ == '__main__':
    charfinder.run(host='0.0.0.0', port=8000, auto_reload=True, debug=True)
