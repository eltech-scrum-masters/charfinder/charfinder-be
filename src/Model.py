from sanic_openapi import doc


class Ability:
    name = doc.String(description='Ability name')
    score = doc.Integer()
    temp_modifier = doc.Integer()
    temp_adjustment = doc.Integer()


class Domain:
    domain_id = doc.Integer()
    slot = doc.Integer()
    name = doc.String()
    description = doc.String()


class Feat:
    feat_id = doc.Integer()
    name = doc.String()
    description = doc.String()


class GearItem:
    item_id = doc.Integer()
    item_name = doc.String()
    item_weight = doc.Float()


class Info:
    full_name = doc.String()
    alignment = doc.String()
    level = doc.Integer()
    experience_points = doc.Integer()
    next_level = doc.Integer()
    deity = doc.String()
    homeland = doc.String()
    race = doc.String()
    size = doc.String()
    gender = doc.String()
    age = doc.Integer()
    height = doc.Float()
    weight = doc.Float()
    hair = doc.String()
    eyes = doc.String()


class Language:
    language_id = doc.Integer()
    name = doc.String()


class Currency:
    currency_id = doc.Integer()
    name = doc.String()
    amount = doc.Integer()


class Skill:
    skill_id = doc.Integer()
    name = doc.String()
    ability_modifier = doc.Integer()
    ranks = doc.Integer()
    misc_modifier = doc.Integer()
    total = doc.Integer()


class Spell:
    spell_id = doc.Integer()
    spell_known = doc.Integer()
    spell_save_dc = doc.Integer()
    level = doc.Integer()
    spells_per_day = doc.Integer()
    bonus_spells = doc.Integer()


class Stat:
    stats_id = doc.Integer()
    initiative = doc.Integer()
    base_attack_bonus = doc.Integer()
    spell_resistance = doc.Integer()


class SpecialAbility:
    name = doc.String()
    description = doc.String()


class ArmorClass:
    armor_class_id = doc.Integer()
    shield_bonus = doc.Integer()
    dex_modifier = doc.Integer()
    size_modifier = doc.Integer()
    natural_armor = doc.Integer()
    deflection_modifier = doc.Integer()
    misc_modifier = doc.Integer()
    touch = doc.Integer()
    flatfooted = doc.Integer()


class Cmb:
    cmb_id = doc.Integer()
    total = doc.Integer()
    base_attack_bonus = doc.Integer()
    strength_modifier = doc.Integer()
    size_modifier = doc.Integer()


class Cmd:
    cmd_id = doc.Integer()
    total = doc.Integer()
    base_attack_bonus = doc.Integer()
    strength_modifier = doc.Integer()
    dexterity_modifier = doc.Integer()
    size_modifier = doc.Integer()


class Health:
    health_id = doc.Integer()
    total_HP = doc.Integer()
    DR = doc.Integer()
    current_HP = doc.Integer()
    nonlethal_damage = doc.String()
    load = doc.String()
    state = doc.String()


class SavingThrow:
    saving_throw_id = doc.Integer()
    name = doc.String()
    total = doc.Integer()
    base_save = doc.Integer()
    ability_modifier = doc.Integer()
    magic_modifier = doc.Integer()
    misc_modifier = doc.Integer()
    temporary_modifier = doc.Integer()


class Speed:
    speed_id = doc.Integer()
    base_speed = doc.Integer()
    with_armor = doc.Integer()
    maneuverability = doc.Integer()
    swim = doc.Integer()
    climb = doc.Integer()
    burrow = doc.Integer()
