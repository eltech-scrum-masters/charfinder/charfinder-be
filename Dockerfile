FROM python:latest as base
RUN pip install -U pip
COPY requirements.txt requirements.txt

FROM base as temporary
RUN pip download -r requirements.txt -d /vendor/python

FROM base as runtime
COPY --from=temporary /vendor/python /vendor/python
RUN pip install /vendor/python/*

RUN mkdir /charfinder
COPY src /charfinder
EXPOSE 8888
ENTRYPOINT ["python", "/charfinder/app.py"]
